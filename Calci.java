class Calci {

public static int add(int a, int b) {
	int sum  = 0;
	sum = a + b;
	return sum;	
}

public static int subtraction(int a, int b) {
	int sub = 0;
	sub = a - b;
	return sub;
}

public static int multiplication(int a, int b) {
	int mul = 1;
	mul = a * b;
	return mul;
}

public static int division(int a, int b) {
	int quotient;
	quotient = a / b;
	return quotient;
}

public static int remainder(int a, int b) {
	int rem;
	rem = a % b;
	return rem;
}

}
